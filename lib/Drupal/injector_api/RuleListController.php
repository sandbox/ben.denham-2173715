<?php

/**
 * Contains \Drupal\injector_api\RuleListController.
 */

namespace Drupal\injector_api;

use Drupal\Core\Config\Entity\ConfigEntityListController;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormInterface;

/**
 * Provides a listing of injector_api rules.
 */
class RuleListController extends ConfigEntityListController implements FormInterface {

  /**
   * {@inheritdoc}
   */
  public function getFormID() {
    return 'injector_api_admin_rules_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $row = parent::buildHeader();
    unset($row['id']);
    $row['weight'] = t('Weight');

    // Re-construct the array as we need to add the description column after the
    // name column.
    $front = array(
      'label' => t('Name'),
      'description' => t('Description'),
    );

    return array_merge($front, $row);
  }

  /**
   * {@inheritdoc}
   */
  public function getOperations(EntityInterface $entity) {
    $operations = parent::getOperations($entity);
    $uri = $entity->uri();

    // Edit operation.
    $operations['edit'] = array(
      'title' => t('Edit'),
      'href' => $uri['path'] . '/edit',
      'options' => $uri['options'],
      'weight' => 10,
    );

    // Delete operation.
    $operations['delete'] = array(
      'title' => t('Delete'),
      'href' => $uri['path'] . '/delete',
      'options' => $uri['options'],
      'weight' => 20,
    );

    return $operations;
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row = parent::buildRow($entity);

    // Override default values to markup elements.
    $row['#attributes']['class'][] = 'draggable';
    unset($row['id']);

    // Re-construct the array as we need to add the description column after the
    // name column.
    $front = array(
      'label' => array(
        '#markup' => check_plain($entity->get('label')),
      ),
      'description' => array(
        '#markup' => check_plain($entity->get('description')),
      ),
    );

    $row['#weight'] = $entity->get('weight');
    // Add weight column.
    $row['weight'] = array(
      '#type' => 'weight',
      '#title' => t('Weight for @title', array('@title' => $entity->label())),
      '#title_display' => 'invisible',
      '#default_value' => $entity->get('weight'),
      '#attributes' => array('class' => array('weight')),
    );

    return array_merge($front, $row);
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    return drupal_get_form($this);
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, array &$form_state) {
    $form['entities'] = array(
      '#type' => 'table',
      '#header' => $this->buildHeader(),
      '#empty' => t("There are no @label's defined yet. <a href='@link'>Add @label</a>", array('@label' => $this->entityInfo['label'], '@link' => 'admin/config/development/injector/add-rule')),
      '#tabledrag' => array(
        array('order', 'sibling', 'weight'),
      ),
    );

    foreach ($this->load() as $entity) {
      $form['entities'][$entity->id()] = $this->buildRow($entity);
    }

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save order'),
      '#button_type' => 'primary',
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, array &$form_state) {
    // No validation.
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, array &$form_state) {
    $values = $form_state['values']['entities'];

    $entities = entity_load_multiple($this->entityType, array_keys($values));
    foreach ($values as $id => $value) {
      if (isset($entities[$id]) && $value['weight'] != $entities[$id]->get('weight')) {
        // Update changed weight.
        $entities[$id]->set('weight', $value['weight']);
        $entities[$id]->save();
      }
    }

    drupal_set_message(t('The rule settings have been updated.'));
  }
}
