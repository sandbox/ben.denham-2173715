<?php

/**
 * @file
 * Contains \Drupal\injector_api\RuleFormController.
 */

namespace Drupal\injector_api;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityFormController;

/**
 * Form controller for the shortcut set entity edit forms.
 */
class RuleFormController extends EntityFormController {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, array &$form_state) {
    $entity = $this->entity;
    $form['label'] = array(
      '#type' => 'textfield',
      '#title' => t('Rule name'),
      '#default_value' => $entity->label(),
      '#size' => 30,
      '#required' => TRUE,
      '#maxlength' => 64,
      '#description' => t('The name for this rule.'),
    );
    $form['id'] = array(
      '#type' => 'machine_name',
      '#default_value' => $entity->id(),
      '#required' => TRUE,
      '#disabled' => !$entity->isNew(),
      '#size' => 30,
      '#maxlength' => 64,
      '#machine_name' => array(
        'exists' => 'injector_api_rule_load',
      ),
    );
    $form['weight'] = array(
      '#type' => 'value',
      '#value' => $entity->get('weight'),
    );

    $form['description'] = array(
      '#type' => 'textfield',
      '#title' => t('Description'),
      '#default_value' => $entity->label(),
      '#size' => 128,
      '#required' => TRUE,
      '#maxlength' => 128,
      '#description' => t('This is to help your fellow administrators as to what this rule does in more detail.'),
    );

    // page visibility fieldset. Code ported from googleanalytics.module
    $form['pages'] = array(
      '#type' => 'fieldset',
      '#title' => t('Pages'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );
    $title = t('Pages');
    $description = t("Specify pages by using their paths. Enter one path per line. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.", array('%blog' => 'blog', '%blog-wildcard' => 'blog/*', '%front' => '<front>'));
    $options = array(
      0 => t('Every page except the listed pages'),
      1 => t('The listed pages only'),
    );
    if (module_exists('php') && user_access('use PHP for settings')) {
      $options[] = t('Pages on which this PHP code returns <code>TRUE</code> (experts only)');
      $title = t('Pages or PHP code');
      $description .= ' ' . t('If the PHP option is chosen, enter PHP code between %php. Note that executing incorrect PHP code can break your Drupal site.', array('%php' => '<?php ?>'));
    }
    $form['pages']['page_visibility'] = array(
      '#type' => 'radios',
      '#title' => t('Add tracking to specific pages'),
      '#options' => $options,
      '#default_value' => $entity->get('page_visibility'),
    );
    $form['pages']['page_visibility_pages'] = array(
      '#type' => 'textarea',
      '#title' => $title,
      '#title_display' => 'invisible',
      '#default_value' => $entity->get('page_visibility_pages'),
      '#description' => $description,
      '#rows' => 10,
    );

    $form['actions']['submit']['#value'] = t('Create new rule');

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, array &$form_state) {
    $entity = $this->entity;

    // Prevent leading and trailing spaces in role names.
    $entity->set('label', trim($entity->label()));

    $uri = $entity->uri();
    if ($entity->save() == SAVED_UPDATED) {
      drupal_set_message(t('Rule %label has been updated.', array('%label' => $entity->label())));
      watchdog('injector_api', 'Rule %label has been updated.', array('%label' => $entity->label()), WATCHDOG_NOTICE, l(t('Edit'), $uri['path']));
    }
    else {
      drupal_set_message(t('Rule %label has been added.', array('%label' => $entity->label())));
      watchdog('injector_api', 'Rule %label has been added.', array('%label' => $entity->label()), WATCHDOG_NOTICE, l(t('Edit'), $uri['path']));
    }

    $form_state['redirect'] = 'admin/config/development/injector';
  }

  /**
   * {@inheritdoc}
   */
  public function delete(array $form, array &$form_state) {
    $form_state['redirect'] = 'admin/config/development/injector/manage/' . $this->entity->id() . '/delete';
  }
}
