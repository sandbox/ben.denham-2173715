<?php

/**
 * @file
 * Contains \Drupal\injector_api\RuleManager.
 */

/**
 * @TODO Review if extending from EntityManager is the correct approach.
 * Currently does not work correctly.
 */

namespace Drupal\injector_api;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Language\LanguageManager;
use Drupal\Core\Language\Language;
use Drupal\Core\Plugin\Discovery\AlterDecorator;
use Drupal\Core\Plugin\Discovery\CacheDecorator;
use Drupal\Core\Plugin\Discovery\AnnotatedClassDiscovery;
use Drupal\Core\Plugin\Discovery\InfoHookDecorator;
use Drupal\Core\StringTranslation\TranslationInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Manages rule entity type plugin definitions.
 */
class RuleManager extends EntityManager {

  /**
   * Constructs a new Rule entity plugin manager.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations,
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The service container this object should use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The cache backend to use.
   * @param \Drupal\Core\Language\LanguageManager $language_manager
   *   The language manager.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $translation_manager
   *   The string translationManager.
   */
  public function __construct(\Traversable $namespaces, ContainerInterface $container, ModuleHandlerInterface $module_handler, CacheBackendInterface $cache, LanguageManager $language_manager, TranslationInterface $translation_manager) {
      parent::__construct($namespaces, $container, $module_handler, $cache, $language_manager, $translation_manager);

    $this->discovery = new AnnotatedClassDiscovery('Rule', $namespaces, 'Drupal\injector_api\Annotation\RuleAnnotation');
    $this->discovery = new InfoHookDecorator($this->discovery, 'injector_rule_info');
    $this->discovery = new AlterDecorator($this->discovery, 'injector_rule_info');
    $this->discovery = new CacheDecorator($this->discovery, 'injector_rule_info:' . $this->languageManager->getLanguage(Language::TYPE_INTERFACE)->id, 'cache', CacheBackendInterface::CACHE_PERMANENT, array('injector_rule_info' => TRUE));
  }

}
