<?php

/**
 * @file
 * Contains \Drupal\injector_api\Entity\RuleBase.
 */

namespace Drupal\injector_api\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\EntityStorageControllerInterface;
use Drupal\injector_api\RuleInterface;

abstract class RuleBase extends ConfigEntityBase implements RuleInterface {

  /**
   * The machine name of this rule.
   *
   * @var string
   */
  public $id;

  /**
   * The UUID of this rule.
   *
   * @var string
   */
  public $uuid;

  /**
   * The human-readable label of this rule.
   *
   * @var string
   */
  public $label;

  /**
   * The weight of this rule in administrative listings.
   *
   * @var int
   */
  public $weight;

  /**
   * The human-readable description of this rule.
   *
   * @var string
   */
  public $description;

  /**
   * Whether the rule has a white or black list for page visibility.
   *
   * @var bool
   */
  public $page_visibility;

  /**
   * A list of pages to either hide or apply the rule.
   *
   * @var string
   */
  public $page_visibility_pages;

  /**
   * Overrides \Drupal\Core\Entity\Entity::uri().
   */
  public function uri() {
    return array(
      'path' => 'admin/config/development/injector/manage/' . $this->id(),
      'options' => array(
        'entity_type' => $this->entityType,
        'entity' => $this,
      ),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageControllerInterface $storage_controller) {
    if (!isset($this->weight) && ($rules = $storage_controller->loadMultiple())) {
      // Set a role weight to make this new role last.
      $max = array_reduce($rules, function($max, $rule) {
        return $max > $rule->weight ? $max : $rule->weight;
      });
      $this->weight = $max + 1;
    }
  }
}
