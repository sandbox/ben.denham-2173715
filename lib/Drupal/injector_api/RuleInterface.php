<?php

/**
 * @file
 * Contains \Drupal\injector_api\RuleInterface.
 */

namespace Drupal\injector_api;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining an injector rule entity.
 */
interface RuleInterface extends ConfigEntityInterface {

  public function inject(&$page);

}
