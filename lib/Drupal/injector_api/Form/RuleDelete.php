<?php

/**
 * @file
 * Contains \Drupal\injector_api\Form\RuleDelete.
 */

namespace Drupal\injector_api\Form;

use Drupal\Core\Entity\EntityConfirmFormBase;

/**
 * Provides a deletion confirmation form for injector rule entity.
 */
class RuleDelete extends EntityConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return t('Are you sure you want to delete the rule %name?', array('%name' => $this->entity->label()));
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelPath() {
    return 'admin/config/development/injector';
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function submit(array $form, array &$form_state) {
    $this->entity->delete();
    watchdog('user', 'Rule %name has been deleted.', array('%name' => $this->entity->label()));
    drupal_set_message(t('Rule %name has been deleted.', array('%name' => $this->entity->label())));
    $form_state['redirect'] = 'admin/config/development/injector';
  }
}
