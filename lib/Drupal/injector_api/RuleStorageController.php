<?php

/**
 * @file
 * Contains \Drupal\injector_api\RuleStorageController.
 */

namespace Drupal\injector_api;

use Drupal\Core\Config\Entity\ConfigStorageController;
use Drupal\Core\Entity\EntityInterface;

/**
 * Controller class for injector_api rules.
 */
class RuleStorageController extends ConfigStorageController {

  /**
   * {@inheritdoc}
   */
  protected function attachLoad(&$queried_entities, $revision_id = FALSE) {
    // Sort the queried roles by their weight.
    uasort($queried_entities, 'Drupal\Core\Config\Entity\ConfigEntityBase::sort');

    parent::attachLoad($queried_entities, $revision_id);
  }
}
