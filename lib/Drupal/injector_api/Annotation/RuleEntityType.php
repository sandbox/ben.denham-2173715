<?php

/**
 * @file
 * Contains \Drupal\injector_api\Annotation\RuleEntityType.
 */

/**
 * @TODO Review if extending from EntityType is the correct approach.
 * Currently does not work correctly.
 */

namespace Drupal\injector_api\Annotation;

use Drupal\Core\Entity\Annotation\EntityType;

/**
 * Defines a Rule entity type annotation object.
 *
 * @Annotation
 */
class RuleEntityType extends EntityType {

  public function __construct($values)
  {
    parent::__construct($values);
    $this->label = t('Rule');
    $this->controllers['storage'] = 'Drupal\injector_api\RuleStorageController';
    $this->controllers['list'] = 'Drupal\injector_api\RuleListController';
    $this->controllers['form'] = array(
        'default' => 'Drupal\injector_api\RuleFormController',
        'delete' => 'Drupal\injector_api\Form\RuleDelete',
    );
    $this->entity_keys['id'] = 'id';
    $this->entity_keys['label'] = 'label';
    $this->entity_keys['uuid'] = 'uuid';
  }

}
